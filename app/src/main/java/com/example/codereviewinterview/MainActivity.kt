package com.example.codereviewinterview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.codereviewinterview.ui.main.OrdersFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, OrdersFragment())
                .commitNow()
        }
    }
}
