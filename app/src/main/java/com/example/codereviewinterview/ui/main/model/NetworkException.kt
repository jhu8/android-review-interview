package com.example.codereviewinterview.ui.main.model

data class NetworkException(override val message: String = ""): Exception()
