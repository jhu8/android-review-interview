package com.example.codereviewinterview.ui.main

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codereviewinterview.ui.main.model.Order
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class OrdersViewModel(context: Context) : ViewModel() {
    private val repository: OrdersRepository = OrdersRepository(context, viewModelScope)

    val isLoadingFlow = MutableStateFlow(false)
    val fullOrdersFlow: MutableStateFlow<List<Order>> = MutableStateFlow(emptyList())

    private val orderId: MutableStateFlow<String?> = MutableStateFlow(null)

    fun getOrders() {
        isLoadingFlow.value = true

        repository.getOrders()
        viewModelScope.launch {
            repository.ordersFlow.collect { orders ->
                orders?.orderIds?.forEach { id ->
                    orderId.tryEmit(id)
                }
            }
        }

        viewModelScope.launch {
            orderId.collect { id ->
                id?.let {
                    repository.getOrder(id)
                }
            }
        }

        viewModelScope.launch {
            repository.orderFlow.collect { order ->
                order?.let {
                    fullOrdersFlow.value = fullOrdersFlow.value + order
                }
            }
        }

        isLoadingFlow.value = false
    }
}
