package com.example.codereviewinterview.ui.main.model

data class Orders(
    val orderIds: List<String>
)
