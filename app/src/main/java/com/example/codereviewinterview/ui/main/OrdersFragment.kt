package com.example.codereviewinterview.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.codereviewinterview.R
import com.example.codereviewinterview.databinding.FragmentOrdersBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class OrdersFragment : Fragment() {

    private lateinit var viewModel: OrdersViewModel
    private lateinit var binding: FragmentOrdersBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = OrdersViewModel(requireContext())
        viewModel.getOrders()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOrdersBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewModelScope.launch {
            launch {
                viewModel.isLoadingFlow.collect { isLoading ->
                    binding.progressBar.isVisible = isLoading
                }
            }
            viewModel.fullOrdersFlow.collect {
                binding.ordersRecyclerView.adapter = OrderItemsAdapter(it)
            }
        }

        binding.refreshFab.setOnClickListener {
            viewModel.getOrders()
        }
    }
}
