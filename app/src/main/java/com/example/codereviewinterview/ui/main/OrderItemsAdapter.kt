package com.example.codereviewinterview.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.codereviewinterview.databinding.ItemOrderItemBinding
import com.example.codereviewinterview.ui.main.model.Order

class OrderItemsAdapter(
    private val orders: List<Order>
) : RecyclerView.Adapter<OrderItemsAdapter.OrderItemViewHolder>() {
    override fun getItemCount(): Int {
        return orders.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderItemViewHolder {
        return OrderItemViewHolder(
            ItemOrderItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: OrderItemViewHolder, position: Int) {
        holder.setItem(position)
    }

    inner class OrderItemViewHolder(
        private val binding: ItemOrderItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun setItem(index: Int) {
            val item = orders[index]
            with(binding) {
                orderIdText.text = "Order #: ${item.orderId}"
                itemsContainer.apply {
                    removeAllViews()
                    item.items.forEach {
                        val textView = TextView(binding.root.context)
                        textView.text = it.name
                        textView.setTextColor(it.color)
                        addView(textView)
                    }
                }
            }
        }
    }
}
