package com.example.codereviewinterview.ui.main.model

data class Order(
    val orderId: String,
    val items: List<Item>
)
