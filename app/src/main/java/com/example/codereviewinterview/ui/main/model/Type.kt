package com.example.codereviewinterview.ui.main.model

sealed class Type {
    object Food: Type()
    object Electronics: Type()
    object Stationary: Type()
    object Unknown: Type()
}
