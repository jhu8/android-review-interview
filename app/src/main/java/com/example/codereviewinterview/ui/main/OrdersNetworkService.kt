package com.example.codereviewinterview.ui.main

import android.content.Context
import com.example.codereviewinterview.R
import com.example.codereviewinterview.ui.main.model.Item
import com.example.codereviewinterview.ui.main.model.NetworkException
import com.example.codereviewinterview.ui.main.model.Order
import com.example.codereviewinterview.ui.main.model.Orders
import com.example.codereviewinterview.ui.main.model.Type
import kotlinx.coroutines.delay
import kotlin.random.Random

class OrdersNetworkService(
    private val context: Context
) {
    private var retryCount = 0

    val networkError = false

    suspend fun getOrders(): Orders {
        delay(2000)
        if (networkError) {
            throw NetworkException()
        } else {
            // Fake network results
            return Orders(
                orderIds = listOf(SAMPLE_ORDER_ID_1, SAMPLE_ORDER_ID_2, SAMPLE_ORDER_ID_3)
            )
        }
    }

    suspend fun getOrder(orderId: String?): Order {
        if (orderId == null) {
            return Order("", emptyList())
        }

        // Fake network call with delay
        delay(Random.nextLong(500, 1500))
        return if (networkError) {
            if (retryCount < 3) {
                retryCount++
                getOrder(orderId)
            } else {
                Order("", emptyList())
            }
        } else {
            retryCount = 0
            // Fake network results
            return when (orderId) {
                SAMPLE_ORDER_ID_1 -> Order(
                    SAMPLE_ORDER_ID_1,
                    items = listOf(
                        Item(
                            "Laptop",
                            "gray".toColors(),
                            "electronics".toType()
                        )
                    )
                )
                SAMPLE_ORDER_ID_2 -> Order(
                    SAMPLE_ORDER_ID_2,
                    items = listOf(
                        Item(
                            "Pencil",
                            "yellow".toColors(),
                            "stationary".toType()
                        )
                    )
                )
                SAMPLE_ORDER_ID_3 -> Order(
                    SAMPLE_ORDER_ID_3,
                    items = listOf(
                        Item("Apple", "red".toColors(), "food".toType()),
                        Item("Grape", "purple".toColors(), "food".toType())
                    )
                )
                else -> Order("", emptyList())
            }
        }
    }

    private fun String.toColors(): Int {
        return when (this) {
            "Red" -> context.getColor(R.color.item_color_red)
            "Purple" -> context.getColor(R.color.item_color_purple)
            "Gray" -> context.getColor(R.color.item_color_gray)
            "Yellow" -> context.getColor(R.color.item_color_yellow)
            else -> context.getColor(R.color.black)
        }
    }

    private fun String.toType(): Type {
        return when (this) {
            "food" -> Type.Food
            "electronics" -> Type.Electronics
            "stationary" -> Type.Stationary
            else -> Type.Unknown
        }
    }

    companion object {
        private const val SAMPLE_ORDER_ID_1 = "order_1"
        private const val SAMPLE_ORDER_ID_2 = "order_2"
        private const val SAMPLE_ORDER_ID_3 = "order_3"
    }
}
