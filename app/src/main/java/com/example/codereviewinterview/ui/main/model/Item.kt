package com.example.codereviewinterview.ui.main.model

data class Item(
    val name: String,
    val color: Int,
    val type: Type
)
