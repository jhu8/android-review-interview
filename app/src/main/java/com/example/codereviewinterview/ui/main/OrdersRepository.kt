package com.example.codereviewinterview.ui.main

import android.content.Context
import com.example.codereviewinterview.ui.main.model.Order
import com.example.codereviewinterview.ui.main.model.Orders
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class OrdersRepository(
    context: Context,
    private val scope: CoroutineScope
) {
    val ordersFlow: MutableStateFlow<Orders?> = MutableStateFlow(null)
    val orderFlow: MutableStateFlow<Order?> = MutableStateFlow(null)

    private val networkService: OrdersNetworkService = OrdersNetworkService(context)

    fun getOrders() {
        scope.launch {
            val orders = networkService.getOrders()
            ordersFlow.emit(orders)
        }
    }

    fun getOrder(orderId: String) {
        scope.launch {
            val order = networkService.getOrder(orderId)
            orderFlow.emit(order)
        }
    }
}
